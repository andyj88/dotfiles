map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

set ruler

nnoremap ; :

set ignorecase
set hlsearch
set incsearch

let mapleader = "-"

set showmatch
set mat=4

set backspace=indent,eol,start
set encoding=utf-8

set visualbell           " don't beep
set noerrorbells         " don't beep

colorscheme elflord
syntax enable

filetype indent plugin on

set tabstop=4                               " 4 whitespaces for tabs visual presentation
set shiftwidth=4                            " shift lines by 4 spaces
set smarttab                                " set tabs for a shifttabs logic
set expandtab                               " expand tabs into spaces
set autoindent                              " indent when moving to the next line while writing code

set laststatus=2

set foldmethod=indent
set foldlevel=99

set nu

set textwidth=79
set clipboard=unnamed

" PEP8 indentation
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=90 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

" full stack indentation
au BufNewFile,BufRead *.js, *.html, *.css
	\ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2

" flag unnecessary whitespace
highlight BadWhitespace ctermbg=red guibg=darkred
au BufNewFile,BufRead *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

let python_highlight_all=1

set mouse=a
